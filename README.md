# Email Prodcaster Frontend

Email brodcast portal using MailJet.

## perquisites

- nodejs v12
- vue-cli

## Installation

- clone this repo

```shell
git clone https://gitlab.com/email-brodcaster/email-brodcaster-frontend.git
```

- install dependencies

```shell
npm i
```

## Start the server

- starting the server in development mode

```shell
npm start
```

## Run using docker

- Run full-stack by cloning the [backend-repo](https://gitlab.com/email-brodcaster/email-brodcaster-backend). Make sure to put the `email-brodcaster-frontend/` and the `email-brodcaster-backend/` on the same directory

- Copy [docker-compose.yml](./docker-compose.yml)

  ```shell
  cp docker-compose.yml ..
  cd ..
  ```

- Run the full-stack project using only one command

  ```shell
  docker-compose up -d
  ```

- Pull images from [dockerHub](https://hub.docker.com/r/yehia67/emailbrodcaster/)

- try it [live](http://bucketsforbrodcast.s3-website-us-east-1.amazonaws.com/)
